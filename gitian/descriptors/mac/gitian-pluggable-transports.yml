---
name: "pluggable-transports-mac"
distro: "debian"
suites:
- "jessie"
architectures:
- "amd64"
packages:
- "faketime"
- "unzip"
- "pkg-config"
- "zip"
reference_datetime: "2000-01-01 00:00:00"
remotes:
- "url": "https://git.torproject.org/pluggable-transports/goptlib.git"
  "dir": "goptlib"
- "url": "https://git.torproject.org/pluggable-transports/meek.git"
  "dir": "meek"
- "url": "https://github.com/agl/ed25519.git"
  "dir": "ed25519"
- "url": "https://github.com/dchest/siphash.git"
  "dir": "siphash"
- "url": "https://go.googlesource.com/crypto"
  "dir": "goxcrypto"
- "url": "https://go.googlesource.com/net"
  "dir": "goxnet"
- "url": "https://git.torproject.org/pluggable-transports/obfs4.git"
  "dir": "obfs4"
- "url": "https://github.com/keroserene/go-webrtc.git"
  "dir": "go-webrtc"
- "url": "https://git.torproject.org/pluggable-transports/snowflake.git"
  "dir": "snowflake"
- "url": "https://github.com/dchest/uniuri.git"
  "dir": "uniuri"
files:
- "versions"
- "go14.tar.gz"
- "go.tar.gz"
- "clang-linux64-jessie-utils.zip"
- "cctools.tar.gz"
- "MacOSX10.7.sdk.tar.gz"
- "webrtc-mac64-gbuilt.zip"
- "dzip.sh"
script: |
  INSTDIR="$HOME/install"
  source versions
  TBDIR="$INSTDIR/TorBrowserBundle.app"
  if [ "z$DATA_OUTSIDE_APP_DIR" = "z1" ]; then
    PTDIR="$TBDIR/Contents/MacOS/Tor/PluggableTransports"
    DOCSDIR="$TBDIR/Contents/Resources/TorBrowser/Docs"
    TORBINDIR="$TBDIR/Contents/MacOS/Tor"
  else
    PTDIR="$TBDIR/TorBrowser/Tor/PluggableTransports"
    DOCSDIR="$TBDIR/TorBrowser/Docs"
    TORBINDIR="$TBDIR/TorBrowser/Tor"
  fi
  export REFERENCE_DATETIME
  export TZ=UTC
  export LC_ALL=C
  umask 0022
  #
  mkdir -p $PTDIR/
  mkdir -p $OUTDIR/
  #
  tar xaf cctools.tar.gz
  unzip clang-linux64-jessie-utils.zip
  # The 10.7 SDK is needed for Go: https://bugs.torproject.org/20023#comment:6
  tar xaf MacOSX10.7.sdk.tar.gz
  # Preparing clang for cross-compilation, setting the proper flags and
  # variables
  # "go link" expects to find a program called "dsymutil" exactly.
  ln -sf x86_64-apple-darwin10-dsymutil $HOME/build/cctools/bin/dsymutil
  # ld needs libLTO.so from llvm
  export LD_LIBRARY_PATH="$HOME/build/clang/lib"
  export PATH="$HOME/build/cctools/bin:$PATH"
  CROSS_CCTOOLS_PATH="$HOME/build/cctools"
  CROSS_SYSROOT="$HOME/build/MacOSX10.7.sdk"
  FLAGS="-target x86_64-apple-darwin10 -mlinker-version=136 -B $CROSS_CCTOOLS_PATH/bin -isysroot $CROSS_SYSROOT"
  export CC="$HOME/build/clang/bin/clang $FLAGS"

  # Building go 1.4.x
  # This is needed to bootstrap the go that we actually use
  # https://golang.org/doc/install/source#go14
  tar xvf go14.tar.gz --transform='s,^go\>,go1.4,'
  cd go1.4/src
  # Disable cgo to avoid conflicts with newer GCC. cgo is not needed for the bootstrap go.
  # https://github.com/golang/go/issues/13114#issuecomment-186922245
  # Disable CC etc. that are set up for cross builds.
  CGO_ENABLED=0 CC= CFLAGS= LDFLAGS= ./make.bash
  cd ../..
  export GOROOT_BOOTSTRAP="$PWD/go1.4"

  # Building go
  # Create a cc-for-target script that closes over CC, CFLAGS, and LDFLAGS.
  # Go's CC_FOR_TARGET only allows a command name, not a command with arguments.
  # https://github.com/golang/go/issues/15457
  CC_FOR_TARGET="$(pwd)/cc-for-target"
  echo "#!/bin/sh" > "$CC_FOR_TARGET"
  echo "exec $CC $CFLAGS $LDFLAGS \"\$@\"" >> "$CC_FOR_TARGET"
  chmod +x "$CC_FOR_TARGET"
  # http://golang.org/doc/install/source#environment
  export GOPATH="$HOME/go"
  export GOOS=darwin
  export GOARCH=amd64
  tar xvf go.tar.gz
  cd go/src
  # faketime is needed because clang 3.8.0 on Darwin embeds the timestamps of
  # certain intermediate object files (including those that arise here while
  # compiling the Go runtime itself). Without this, variable timestamps would
  # end up in snowflake-client.
  # https://github.com/golang/go/issues/9206#issuecomment-310476743
  CGO_ENABLED=1 CC_FOR_TARGET="$CC_FOR_TARGET" CC= CFLAGS= LDFLAGS= faketime -f "$REFERENCE_DATETIME" ./make.bash
  cd ../..
  export PATH="$PATH:$PWD/go/bin"

  # Building goptlib
  cd goptlib
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  mkdir -p "$GOPATH/src/git.torproject.org/pluggable-transports"
  ln -sf "$PWD" "$GOPATH/src/git.torproject.org/pluggable-transports/goptlib.git"
  go install git.torproject.org/pluggable-transports/goptlib.git
  cd ..

  # Building meek
  cd meek
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  cd meek-client
  # https://code.google.com/p/go/issues/detail?id=4714#c7
  # We need cgo for crypto/x509 support on mac.
  go build -ldflags '-s'
  cp -a meek-client $PTDIR
  cd ..
  cd meek-client-torbrowser
  go build -ldflags '-s'
  cp -a meek-client-torbrowser $PTDIR
  cd ..
  mkdir -p $DOCSDIR/meek
  cp -a README doc/*.1 $DOCSDIR/meek
  cd ..

  # Building go ed25519
  cd ed25519
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  mkdir -p "$GOPATH/src/github.com/agl/"
  ln -sf "$PWD" "$GOPATH/src/github.com/agl/ed25519"
  go install github.com/agl/ed25519/extra25519
  cd ..

  # Building go siphash
  cd siphash
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  mkdir -p "$GOPATH/src/github.com/dchest/"
  ln -sf "$PWD" "$GOPATH/src/github.com/dchest/siphash"
  go install github.com/dchest/siphash
  cd ..

  # Building go uniuri
  cd uniuri
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  mkdir -p "$GOPATH/src/github.com/dchest/"
  ln -sf "$PWD" "$GOPATH/src/github.com/dchest/uniuri"
  go install github.com/dchest/uniuri
  cd ..

  # Building golang.org/x/crypto (obfs4proxy > 0.0.3 || Go >= 1.4)
  cd goxcrypto
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  mkdir -p "$GOPATH/src/golang.org/x/"
  ln -sf "$PWD" "$GOPATH/src/golang.org/x/crypto"
  go install golang.org/x/crypto/curve25519
  go install golang.org/x/crypto/hkdf
  go install golang.org/x/crypto/nacl/secretbox
  cd ..

  # Building golang.org/x/net (obfs4proxy > 0.0.4)
  cd goxnet
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  mkdir -p "$GOPATH/src/golang.org/x/"
  ln -sf "$PWD" "$GOPATH/src/golang.org/x/net"
  go install golang.org/x/net/proxy
  cd ..

  # Building obfs4proxy
  cd obfs4
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  mkdir -p "$GOPATH/src/git.torproject.org/pluggable-transports"
  ln -sf "$PWD" "$GOPATH/src/git.torproject.org/pluggable-transports/obfs4.git"
  cd obfs4proxy
  go build -ldflags '-s'
  cp -a obfs4proxy $PTDIR
  cd ../..

  unzip webrtc-mac64-gbuilt.zip
  export SDKROOT="$PWD/MacOSX10.7.sdk"

  # Building go-webrtc
  cd go-webrtc
  # Replace the prebuilt webrtc library with our own one.
  rm -rf include/ lib/
  ln -sf ../webrtc/{include,lib} .
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  mkdir -p "$GOPATH/src/github.com/keroserene/"
  ln -sf "$PWD" "$GOPATH/src/github.com/keroserene/go-webrtc"
  CFLAGS="$FLAGS -mmacosx-version-min=10.7"
  CXXFLAGS="$FLAGS -stdlib=libc++ -mmacosx-version-min=10.7"
  LDFLAGS="$FLAGS -stdlib=libc++ -mmacosx-version-min=10.7"
  GOARCH=amd64 CGO_ENABLED=1 CGO_CFLAGS="$CFLAGS" CGO_CXXFLAGS="$CXXFLAGS" CGO_LDFLAGS="$LDFLAGS" CC="$HOME/build/clang/bin/clang" CXX="$HOME/build/clang/bin/clang++" go install github.com/keroserene/go-webrtc
  cd ..

  # Building snowflake
  cd snowflake
  find -type f -print0 | xargs -0 touch --date="$REFERENCE_DATETIME"
  cd client
  # See the faketime comment above. Without faketime, snowflake-client would
  # contain the timestamp of the temporary client.a file created during
  # "go build".
  GOARCH=amd64 CGO_ENABLED=1 CGO_CFLAGS="$CFLAGS" CGO_CXXFLAGS="$CXXFLAGS" CGO_LDFLAGS="$LDFLAGS" CC="$HOME/build/clang/bin/clang" CXX="$HOME/build/clang/bin/clang++" faketime -f "$REFERENCE_DATETIME" go build -ldflags '-s'
  # Hack: Overwrite variable absolute paths embedded in the binary. clang 3.8.0
  # on Darwin embeds such paths and the issue is unsolved in upstream Go as of
  # 2016-06-28:
  # https://github.com/golang/go/issues/9206#issuecomment-310476743
  # The two kinds of paths are ("000000000" stands for 9 random digits):
  #   /tmp/go-build000000000
  #   /tmp/go-link-000000000
  # Such paths are the output of ioutil.TempDir("", "go-build") and
  # ioutil.TempDir("", "go-link-").
  cp -a client client.stomped
  sed -i -E -e 's#(/tmp/go-build|/tmp/go-link-)[0-9]{9}/#\1XXXXXXXXX/#g' client.stomped
  # Sanity check: make sure the file actually changed. If it did not, it could
  # mean that a change in go or clang has made this step unnecessary.
  cmp client client.stomped && (echo "No paths replaced in snowflake-client. Maybe the replacement failed or is no longer needed. Check descriptors/mac/gitian-pluggable-transports.yml"; exit 1)
  cp -a client.stomped $PTDIR/snowflake-client
  cd ..
  mkdir -p $DOCSDIR/snowflake
  cp -a README.md LICENSE $DOCSDIR/snowflake
  cd ..

  # Grabbing the result
  cd $INSTDIR
  ~/build/dzip.sh pluggable-transports-mac64-gbuilt.zip TorBrowserBundle.app
  cp pluggable-transports-mac64-gbuilt.zip $OUTDIR/
